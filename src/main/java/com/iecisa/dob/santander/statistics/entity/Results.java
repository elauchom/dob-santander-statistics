package com.iecisa.dob.santander.statistics.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "results", uniqueConstraints = {@UniqueConstraint(name = "RESULTS_UK", columnNames = "id_request") })
public class Results {
	
	public Results() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Constructor.
	 * @param resultsId
	 * @param idRequest
	 * @param files
	 * @param pages
	 * @param lastUpdate
	 * @param secondsPerTransaction
	 */
	public Results(Long resultsId , String idRequest , Long files , Long pages , LocalDateTime lastUpdate , Long secondsPerTransaction) {
		this.resultsId = resultsId;
		this.idRequest = idRequest;  
		this.files = files; 
		this.pages = pages; 
		this.lastUpdate = lastUpdate;
		this.secondsPerTransaction = secondsPerTransaction;
	}//Fin Constructor.
	
	/**
	 * Constructor.
	 * @param totales
	 * @param mediaFiles
	 * @param mediaPages
	 * @param mediaSecondsPerTransaction
	 * @param resultasMenos60Seg
	 * @param documetnResultNull
	 */
	public Results(Long totales , BigDecimal mediaFiles , BigDecimal mediaPages , BigDecimal mediaSecondsPerTransaction , Long resultasMenos60Seg,  Long documetnResultNull) { 
		this.totales = totales;
		this.mediaFiles = mediaFiles;
		this.mediaPages = mediaPages;
		this.mediaSecondsPerTransaction = mediaSecondsPerTransaction;
		this.resultasMenos60Seg = resultasMenos60Seg;
		this.documetnResultNull = documetnResultNull;
	}//Fin Constructor.

	@Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Long resultsId;
	
	@Column(name = "id_request", length = 100, nullable = false)
    private String idRequest;
	
	@Column(name = "document_result" , columnDefinition = "text")
	private String documentResult;
	
	@Column(name = "files", nullable = false)
    private Long files;
	
	@Column(name = "pages", nullable = false)
    private Long pages;
	
	@Column(name = "last_update", nullable = false)
    private LocalDateTime lastUpdate;
	
	@Column(name = "seconds_per_transaction", nullable = true)
    private Long secondsPerTransaction;
	
	@Transient
	private Long totales ;
	
	@Transient
	private BigDecimal mediaFiles ;
	
	@Transient
	private BigDecimal mediaPages ;
	
	@Transient
	private BigDecimal mediaSecondsPerTransaction ;
	
	@Transient
	private Long resultasMenos60Seg;
	
	@Transient
	private Long documetnResultNull;
	
	/**
	 * @return the resultsId
	 */
	public Long getResultsId() {
		return resultsId;
	}

	/**
	 * @return the idRequest
	 */
	public String getIdRequest() {
		return idRequest;
	}


	/**
	 * @return the files
	 */
	public Long getFiles() {
		return files;
	}

	/**
	 * @return the pages
	 */
	public Long getPages() {
		return pages;
	}

	/**
	 * @return the lastUpdate
	 */
	public LocalDateTime getLastUpdate() {
		return lastUpdate;
	}

	/**
	 * @return the secondsPerTransaction
	 */
	public Long getSecondsPerTransaction() {
		return secondsPerTransaction;
	}

	/**
	 * @param resultsId the resultsId to set
	 */
	public void setResultsId(Long resultsId) {
		this.resultsId = resultsId;
	}

	/**
	 * @param idRequest the idRequest to set
	 */
	public void setIdRequest(String idRequest) {
		this.idRequest = idRequest;
	}

	/**
	 * @param files the files to set
	 */
	public void setFiles(Long files) {
		this.files = files;
	}

	/**
	 * @param pages the pages to set
	 */
	public void setPages(Long pages) {
		this.pages = pages;
	}

	/**
	 * @param lastUpdate the lastUpdate to set
	 */
	public void setLastUpdate(LocalDateTime lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	/**
	 * @param secondsPerTransaction the secondsPerTransaction to set
	 */
	public void setSecondsPerTransaction(Long secondsPerTransaction) {
		this.secondsPerTransaction = secondsPerTransaction;
	}

	/**
	 * @return the documentResult
	 */
	public String getDocumentResult() {
		return documentResult;
	}

	/**
	 * @param documentResult the documentResult to set
	 */
	public void setDocumentResult(String documentResult) {
		this.documentResult = documentResult;
	}

	/**
	 * @return the totales
	 */
	public Long getTotales() {
		return totales;
	}

	/**
	 * @return the mediaFiles
	 */
	public BigDecimal getMediaFiles() {
		return mediaFiles;
	}

	/**
	 * @return the mediaPages
	 */
	public BigDecimal getMediaPages() {
		return mediaPages;
	}

	/**
	 * @return the mediaSecondsPerTransaction
	 */
	public BigDecimal getMediaSecondsPerTransaction() {
		return mediaSecondsPerTransaction;
	}

	/**
	 * @return the resultasMenos60Seg
	 */
	public Long getResultasMenos60Seg() {
		return resultasMenos60Seg;
	}

	/**
	 * @return the documetnResultNull
	 */
	public Long getDocumetnResultNull() {
		return documetnResultNull;
	}

	/**
	 * @param totales the totales to set
	 */
	public void setTotales(Long totales) {
		this.totales = totales;
	}

	/**
	 * @param mediaFiles the mediaFiles to set
	 */
	public void setMediaFiles(BigDecimal mediaFiles) {
		this.mediaFiles = mediaFiles;
	}

	/**
	 * @param mediaPages the mediaPages to set
	 */
	public void setMediaPages(BigDecimal mediaPages) {
		this.mediaPages = mediaPages;
	}

	/**
	 * @param mediaSecondsPerTransaction the mediaSecondsPerTransaction to set
	 */
	public void setMediaSecondsPerTransaction(BigDecimal mediaSecondsPerTransaction) {
		this.mediaSecondsPerTransaction = mediaSecondsPerTransaction;
	}

	/**
	 * @param resultasMenos60Seg the resultasMenos60Seg to set
	 */
	public void setResultasMenos60Seg(Long resultasMenos60Seg) {
		this.resultasMenos60Seg = resultasMenos60Seg;
	}

	/**
	 * @param documetnResultNull the documetnResultNull to set
	 */
	public void setDocumetnResultNull(Long documetnResultNull) {
		this.documetnResultNull = documetnResultNull;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Results [resultsId=");
		builder.append(resultsId);
		builder.append(", idRequest=");
		builder.append(idRequest);
		builder.append(", documentResult=");
		builder.append(documentResult);
		builder.append(", files=");
		builder.append(files);
		builder.append(", pages=");
		builder.append(pages);
		builder.append(", lastUpdate=");
		builder.append(lastUpdate);
		builder.append(", secondsPerTransaction=");
		builder.append(secondsPerTransaction);
		builder.append("]");
		return builder.toString();
	}	
}
