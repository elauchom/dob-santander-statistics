package com.iecisa.dob.santander.statistics.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iecisa.dob.santander.statistics.entity.AppUser;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {
	
	/**
	 * Busca un usuario por su userName.
	 * @param idUser
	 * @return Devuelve objeto AppUser (En caso de encontrar al usuario registrado)
	 */
	AppUser findByUserName(String userName);
}
