package com.iecisa.dob.santander.statistics.services;

import java.time.LocalDateTime;
import java.util.List;
import com.iecisa.dob.santander.statistics.entity.Results;

public interface IStatisticsService {
	/**
	 * Devuelve una lista de resultados filtrados por un rango de fecha
	 * @param fechaDesde
	 * @param fechaHasta
	 * @return List<Results>
	 */
	public List<Results> findByLastUpdateGreaterThanEqualAndLastUpdateLessThanEqualOrderByLastUpdateDesc(
			LocalDateTime fechaDesde, LocalDateTime fechaHasta);
	
	/**
	 * Devuelve un objeto Results que contiene las estadisticas y media aritmetica.
	 * @param fechaDesde
	 * @param fechaHasta
	 * @return List<Results>
	 */
	public Object getStatistics(LocalDateTime fechaDesde, LocalDateTime fechaHasta);
}
