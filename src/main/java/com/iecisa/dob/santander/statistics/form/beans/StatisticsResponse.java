package com.iecisa.dob.santander.statistics.form.beans;

import java.math.BigDecimal;

public class StatisticsResponse{

	/*** Totales de registros de la tabla Results*/
	private Long total;
	
	/*** Numero medio de archivos enviados por llamada  */
	private BigDecimal mediaFiles;

	/*** Numero medio de páginas enviados por llamada  */
	private BigDecimal mediaPages;

	/*** Tiempo medio de respuesta  */
	private BigDecimal mediaSecondsPerTransaction;

	/*** Transacciones realizadas en menos de 60 segundos. */
	private Long resultasMenos60Seg;

	/*** Llamadas que no han obtenido respuestas (DocumentResult NULL) */
	private Long documentResultNull;
	
	/*** Porcentaje de las llamadas que se han resuelto dentro del tiempo establecido de 60 segundos*/
	private BigDecimal porcentajeTiempoEstablecido;
	
	/*** Porcentaje de las llamadas que no han obtenido respuesta DOcument result vacio*/
	private BigDecimal porcentajeDocumentResultNull;

	/**
	 * @return the total
	 */
	public Long getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(Long total) {
		this.total = total;
	}

	/**
	 * @return the mediaFiles
	 */
	public BigDecimal getMediaFiles() {
		return mediaFiles;
	}

	/**
	 * @return the mediaPages
	 */
	public BigDecimal getMediaPages() {
		return mediaPages;
	}

	/**
	 * @return the mediaSecondsPerTransaction
	 */
	public BigDecimal getMediaSecondsPerTransaction() {
		return mediaSecondsPerTransaction;
	}

	/**
	 * @return the resultasMenos60Seg
	 */
	public Long getResultasMenos60Seg() {
		return resultasMenos60Seg;
	}

	/**
	 * @return the documentResultNull
	 */
	public Long getDocumentResultNull() {
		return documentResultNull;
	}

	/**
	 * @param mediaFiles the mediaFiles to set
	 */
	public void setMediaFiles(BigDecimal mediaFiles) {
		this.mediaFiles = mediaFiles;
	}

	/**
	 * @param mediaPages the mediaPages to set
	 */
	public void setMediaPages(BigDecimal mediaPages) {
		this.mediaPages = mediaPages;
	}

	/**
	 * @param mediaSecondsPerTransaction the mediaSecondsPerTransaction to set
	 */
	public void setMediaSecondsPerTransaction(BigDecimal mediaSecondsPerTransaction) {
		this.mediaSecondsPerTransaction = mediaSecondsPerTransaction;
	}

	/**
	 * @param resultasMenos60Seg the resultasMenos60Seg to set
	 */
	public void setResultasMenos60Seg(Long resultasMenos60Seg) {
		this.resultasMenos60Seg = resultasMenos60Seg;
	}

	/**
	 * @param documentResultNull the documentResultNull to set
	 */
	public void setDocumentResultNull(Long documentResultNull) {
		this.documentResultNull = documentResultNull;
	}

	/**
	 * @return the porcentajeTiempoEstablecido
	 */
	public BigDecimal getPorcentajeTiempoEstablecido() {
		return porcentajeTiempoEstablecido;
	}

	/**
	 * @return the porcentajeDocumenResultNull
	 */
	public BigDecimal getPorcentajeDocumentResultNull() {
		return porcentajeDocumentResultNull;
	}

	/**
	 * @param porcentajeTiempoEstablecido the porcentajeTiempoEstablecido to set
	 */
	public void setPorcentajeTiempoEstablecido(BigDecimal porcentajeTiempoEstablecido) {
		this.porcentajeTiempoEstablecido = porcentajeTiempoEstablecido;
	}

	/**
	 * @param porcentajeDocumenResultNull the porcentajeDocumenResultNull to set
	 */
	public void setPorcentajeDocumentResultNull(BigDecimal porcentajeDocumentResultNull) {
		this.porcentajeDocumentResultNull = porcentajeDocumentResultNull;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("StatisticsResponse [total=");
		builder.append(total);
		builder.append(", mediaFiles=");
		builder.append(mediaFiles);
		builder.append(", mediaPages=");
		builder.append(mediaPages);
		builder.append(", mediaSecondsPerTransaction=");
		builder.append(mediaSecondsPerTransaction);
		builder.append(", resultasMenos60Seg=");
		builder.append(resultasMenos60Seg);
		builder.append(", documentResultNull=");
		builder.append(documentResultNull);
		builder.append(", porcentajeTiempoEstablecido=");
		builder.append(porcentajeTiempoEstablecido);
		builder.append(", porcentajeDocumenResultNull=");
		builder.append(porcentajeDocumentResultNull);
		builder.append("]");
		return builder.toString();
	}
}
