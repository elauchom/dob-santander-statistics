package com.iecisa.dob.santander.statistics.controller;

import java.security.Principal;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.iecisa.dob.santander.statistics.utils.WebUtils;
 
@Controller
public class LoginController {
 
    @RequestMapping(value = { "/", "/login" }, method = RequestMethod.GET)
    public String loginPage(Model model) {
        return "loginPage";
    }
 
    @RequestMapping(value = "/logoutSuccessful", method = RequestMethod.GET)
    public String logoutSuccessfulPage(Model model) {
        model.addAttribute("title", "Logout");
        return "logoutSuccessfulPage";
    }

    /**
     * Usuario NO tiene permisos para acceder a la URL.
     * @param model
     * @param principal
     * @return Devuelve pagina 403.
     */
    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public String accessDenied(Model model, Principal principal) {
 
        if (principal != null) {
            User loginedUser = (User) ((Authentication) principal).getPrincipal();
            
            String userInfo = WebUtils.toString(loginedUser);
            model.addAttribute("userInfo", userInfo);
            String message = "Hola " + principal.getName() + "<br> Permiso denegado para acceder a esta p&aacute;gina!";
            model.addAttribute("message", message);
        }
 
        return "403Page";
    }//Fin accessDenied
}
