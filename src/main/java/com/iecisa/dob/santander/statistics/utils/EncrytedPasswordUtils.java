package com.iecisa.dob.santander.statistics.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class EncrytedPasswordUtils {
	// Encripta la password con  BCryptPasswordEncoder
    public static String encrytePassword(String password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(password);
    }
    
    public static void main(String[] args) {
    	System.out.println(EncrytedPasswordUtils.encrytePassword("54nT4#d3RI9.$"));
	}
}
