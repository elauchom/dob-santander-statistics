package com.iecisa.dob.santander.statistics.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.iecisa.dob.santander.statistics.entity.AppRole;
public interface AppRoleRepository extends JpaRepository<AppRole, Long>{

}
