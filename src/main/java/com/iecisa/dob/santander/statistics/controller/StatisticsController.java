package com.iecisa.dob.santander.statistics.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.iecisa.dob.santander.statistics.entity.Results;
import com.iecisa.dob.santander.statistics.form.beans.StatisticsBean;
import com.iecisa.dob.santander.statistics.form.beans.StatisticsResponse;
import com.iecisa.dob.santander.statistics.services.IStatisticsService;
import com.iecisa.dob.santander.statistics.utils.WebUtils;

@Controller
public class StatisticsController {

	@Autowired
	IStatisticsService statisticsIService;
	
	/*** Cadena de formato LocalDateTime*/
	private static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
	
	/*** Formato para LocalDateTime*/
	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern(YYYY_MM_DD_HH_MM_SS);

	/**
	 * Inicia la petición BROWSER que muestra los datos de la tabla RESULTS
	 * 
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/browser", method = RequestMethod.GET)
	public String browser(Model model, Principal principal ,  @Valid StatisticsBean statisticsBean) {

		// Usuario logueado
		User loginedUser = (User) ((Authentication) principal).getPrincipal();

		// Información del usuario
		String userInfo = WebUtils.toString(loginedUser);

		// Se envian los datos a la vista.
		model.addAttribute("userInfo", userInfo);// Información del usuario conectado
		model.addAttribute("title", "Estad&iacute;sticas");// Título de la página

		//Los datos se muestran a cargar la página vía AJAX.
		return "browser";
		
	}// Fin browser

	/**
	 * Inicia la petición STATISTICS que muestra los datos de las estadísticas.
	 * 
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/statistics", method = RequestMethod.GET)
	public String getResults(Model model, Principal principal , @Valid StatisticsBean statisticsBean) {

		// Usuario logueado
		User loginedUser = (User) ((Authentication) principal).getPrincipal();

		// Información del usuario
		String userInfo = WebUtils.toString(loginedUser);

		// Se envian los datos a la vista.
		model.addAttribute("userInfo", userInfo);// Información del usuario conectado
		model.addAttribute("title", "Estad&iacute;sticas");// Título de la página

		//Los datos se muestran a cargar la página vía AJAX.
		return "statistics";
	}// Fin getResults

	/**
	 * Devuelve las estadisticas del día actual.
	 * 
	 * @param model
	 * @param principal
	 * @param statisticsBean
	 * @return
	 */
	@GetMapping(path = "/statistics/graphics/ajax", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StatisticsResponse> graphicsAjax(Model model, Principal principal, StatisticsBean statisticsBean) {
		
	// Usuario logueado
	User loginedUser = (User) ((Authentication) principal).getPrincipal();

	// Información del usuario
	String userInfo = WebUtils.toString(loginedUser);

	LocalDateTime fechaHasta = LocalDateTime.now();
    LocalDateTime fechaDesde = fechaHasta.withDayOfMonth(1).withHour(0).withMinute(0);

    //Lista de objeto respuesta
    StatisticsResponse statsResponse = new StatisticsResponse();
    
    //Se agrega a la lista los datos obtenidos de BBDD;
    statsResponse = populate(fechaDesde, fechaHasta);

    // Se envian los datos a la vista.
 	model.addAttribute("userInfo", userInfo);// Información del usuario conectado
	model.addAttribute("title", "Gr&aacute;ficos");// Título de la página

	return ResponseEntity.ok(statsResponse);
			
}// Fin graphicsAjax
	
	/**
	 * 
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/graphics", method = RequestMethod.GET)
	public String graphics(Model model, Principal principal) {
			
		// Usuario logueado
		User loginedUser = (User) ((Authentication) principal).getPrincipal();

		// Información del usuario
		String userInfo = WebUtils.toString(loginedUser);

		// Se envian los datos a la vista.
		model.addAttribute("userInfo", userInfo);// Información del usuario conectado
		
		return "graphics";
				
	}// Fin graphics
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/graphicsDiv", method = RequestMethod.GET)
	public String graphicsDiv() {
		return "graphicsDiv";		
	}// Fin graphics

	/**
	 * Genera las estadisticas y devuelve el fragmento de código a reemplazar en la petición AJAX.
	 * @param model
	 * @param sBean
	 * @return
	 */
	@RequestMapping(value = "/statistics/ajax", method = RequestMethod.POST)
	public String statisticsAjax(Model model , StatisticsBean sBean) {

		LocalDateTime fechaActual = LocalDateTime.now();
		LocalDateTime fechaDesde = LocalDateTime.parse(sBean.getFechaDesde(), formatter);
        LocalDateTime fechaHasta = LocalDateTime.parse(sBean.getFechaHasta(), formatter).withHour(fechaActual.getHour()).withMinute(fechaActual.getMinute());

        //Lista de objeto respuesta
        List<StatisticsResponse> lista = new ArrayList<StatisticsResponse>();
        
        //Se agrega a la lista los datos obtenidos de BBDD
        lista.add(populate(fechaDesde, fechaHasta));

		//Se envian los datos a la vista.
		model.addAttribute("title", "Resultados");// Título de la página
        model.addAttribute("statistics", lista);
        model.addAttribute("percent", " %");

		return "statisticsAjax";
	}// Fin statisticsAjax
	
	@RequestMapping(value = "/browserAjax", method = RequestMethod.POST)
	public String browserAjax(Model model , StatisticsBean sBean) {

		LocalDateTime fechaActual = LocalDateTime.now();
		LocalDateTime fechaDesde = LocalDateTime.parse(sBean.getFechaDesde(), formatter);
        LocalDateTime fechaHasta = LocalDateTime.parse(sBean.getFechaHasta(), formatter).withHour(fechaActual.getHour()).withMinute(fechaActual.getMinute());

		// Consulta a BBDD.
		List<Results> result = statisticsIService
				.findByLastUpdateGreaterThanEqualAndLastUpdateLessThanEqualOrderByLastUpdateDesc(
						fechaDesde, fechaHasta);

		// Se envian los datos a la vista.
		model.addAttribute("title", "Resultados");// Título de la página
		model.addAttribute("statistics", result);// Resultado de la consulta en BD.

		return "browserAjax";
	}// Fin browserAjax
	
	/**
	 * Rellena el objeto StatisticsResponse con información obtenida de BBDD.
	 * 
	 * @return StatisticsBean
	 */
	private StatisticsResponse populate(LocalDateTime fechaDesde, LocalDateTime fechaHasta)
	{
		// Consulta a BBDD.
		Object result = statisticsIService.getStatistics(fechaDesde, fechaHasta);
		
		//Objeto respuesta
		StatisticsResponse response = new StatisticsResponse();
		
		//Populate data
		List<?> list = Arrays.asList((Object[])result);
		
		response.setTotal( Long.valueOf(list.get(0).toString()));
		response.setMediaFiles(getDecimalFormatterValue(list.get(1)));
		response.setMediaPages(getDecimalFormatterValue(list.get(2)));
		response.setMediaSecondsPerTransaction(getDecimalFormatterValue(list.get(3)));
		response.setResultasMenos60Seg(Long.valueOf(list.get(4).toString()));
		response.setDocumentResultNull(Long.valueOf(list.get(5).toString()));
		
		//Establece el valor de los porcentajes.
		setPercentages(response);

		return response;
	}//Fin populate
	
	/**
	 * Devuelve BigDecimal con escala 2 y redondeo cuando sea necesario.
	 * @param obj
	 * @return
	 */
	private BigDecimal getDecimalFormatterValue(Object obj)
	{
		return new BigDecimal(obj.toString()).setScale(2 , RoundingMode.HALF_EVEN);
	}//Fin getDecimalFormatterValue
	
	/**
	 * Establece los siguientes valores:
	 * <ul>
	 * <li>Porcentaje de llamadas que se han resuelto dentro del tiempo establecido de 60 segundos</li>
	 * <li>Porcentaje de las llamadas que no han obtenido respuesta</li>
	 * </ul>
	 * @param response
	 */
	private void setPercentages(StatisticsResponse response)
	{
		/* *********************************************************************************************************
		 * Mostrar el porcentaje de las llamadas que se han resuelto dentro del tiempo establecido de 60 segundos. * 
		 * *********************************************************************************************************
		 */
		BigDecimal registrosTiempoEstablecido = getDecimalFormatterValue(response.getResultasMenos60Seg())
				.multiply(new BigDecimal(100));
		BigDecimal porcentajeTiempoEstablecido = response.getTotal().equals(0l) ? BigDecimal.ZERO
				: registrosTiempoEstablecido.divide(getDecimalFormatterValue(response.getTotal()));

		/* *********************************************************************************************************
		 * Mostrar el porcentaje de las llamadas que no han obtenido respuesta DOcument result vacio               *
		 * *********************************************************************************************************
		 */
		BigDecimal registrosDocumentResulNull = getDecimalFormatterValue(response.getDocumentResultNull())
				.multiply(new BigDecimal(100));
		BigDecimal porcentajeDocumentResultNull = response.getDocumentResultNull().equals(0l) ? BigDecimal.ZERO
				: registrosDocumentResulNull.divide(getDecimalFormatterValue(response.getTotal()));
		
		//Set values
		response.setPorcentajeTiempoEstablecido(porcentajeTiempoEstablecido);
		response.setPorcentajeDocumentResultNull(porcentajeDocumentResultNull);
		
	}//Fin setPercentages
	
}// Fin StatisticsController
