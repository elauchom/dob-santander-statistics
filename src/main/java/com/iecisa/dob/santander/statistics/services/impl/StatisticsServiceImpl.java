package com.iecisa.dob.santander.statistics.services.impl;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.iecisa.dob.santander.statistics.entity.Results;
import com.iecisa.dob.santander.statistics.repository.ResultsRepository;
import com.iecisa.dob.santander.statistics.services.IStatisticsService;

@Service
public class StatisticsServiceImpl implements IStatisticsService {

	@Autowired
	ResultsRepository resultsRepository;

	@Override
	public List<Results> findByLastUpdateGreaterThanEqualAndLastUpdateLessThanEqualOrderByLastUpdateDesc(
			LocalDateTime fechaDesde, LocalDateTime fechaHasta) {
		return resultsRepository.findByLastUpdateGreaterThanEqualAndLastUpdateLessThanEqualOrderByLastUpdateDesc(fechaDesde, fechaHasta);
	}

	@Override
	public Object getStatistics(LocalDateTime fechaDesde, LocalDateTime fechaHasta) {
		return resultsRepository.getStatistics(fechaDesde, fechaHasta);
	}
}//Fin StatisticsServiceImpl
