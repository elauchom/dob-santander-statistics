package com.iecisa.dob.santander.statistics.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.iecisa.dob.santander.statistics.entity.Results;

public interface ResultsRepository extends JpaRepository<Results, Integer> {

	/**
	 * Devuelve una lista de Results que cumplan con las condiciones de los
	 * parámetros.
	 * 
	 * @param servicio Objeto ServiciosMonitorEntity con el identificador.
	 * @param desde    (Fecha inicio)
	 * @param hasta    (Fecha inicio)
	 * @return List <PkiSlaEntity>
	 */
	@Query("SELECT new Results(rs.resultsId , rs.idRequest , rs.files , rs.pages , rs.lastUpdate , rs.secondsPerTransaction)  FROM Results rs where rs.lastUpdate between :fechaDesde and :fechaHasta ORDER BY rs.lastUpdate DESC")
	List<Results> findByLastUpdateGreaterThanEqualAndLastUpdateLessThanEqualOrderByLastUpdateDesc(
			 @Param("fechaDesde") LocalDateTime fechaDesde, @Param("fechaHasta") LocalDateTime fechaHasta);
	
	
	@Query(value = "SELECT count(*) as totales, COALESCE(avg(re.files),0)as files, COALESCE(avg(re.pages),0) as pages, COALESCE(avg(re.seconds_per_transaction),0)as seconds, COUNT(*) filter (where re.seconds_per_transaction <= 60) as min, COUNT(*) filter (where re.document_result = '')as resultnull FROM results re where re.last_update between :fechaDesde and :fechaHasta " , nativeQuery = true)
	Object getStatistics(@Param("fechaDesde") LocalDateTime fechaDesde, @Param("fechaHasta") LocalDateTime fechaHasta);
}// Fin ResultsRepository
