package com.iecisa.dob.santander.statistics.services.impl;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import com.iecisa.dob.santander.statistics.entity.AppUser;
import com.iecisa.dob.santander.statistics.entity.UserRole;
import com.iecisa.dob.santander.statistics.repository.AppUserRepository;
import com.iecisa.dob.santander.statistics.repository.UserRoleRepository;

@Transactional(rollbackFor = { Exception.class })
public class UserDetailsServiceImp implements UserDetailsService {
	
	@Autowired
	AppUserRepository appUserRepository;
	
	@Autowired
	UserRoleRepository  userRoleRepository;
	
	/*** Trazas de la aplicacion para la clase UserDetailsServiceImp.*/
	private static final Logger logger = LogManager.getLogger(UserDetailsServiceImp.class);

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		//Busca en BBDD el username ingresado en el Login.
		AppUser appUser = this.appUserRepository.findByUserName(username);
		
		//Si el usuario NO es encontrado en BBDD se lanza UsernameNotFoundException.
        if (appUser == null) {
            logger.error("User not found! " + username);
            throw new UsernameNotFoundException("User " + username + " was not found in the database");
        }
        
        //Usuario encontrado
        logger.info("Found User: " + appUser);
        
     	//Obtiene la lista de ROLES asociados al usuario que esta haciendo login.
        List<UserRole> roles = this.userRoleRepository.findByAppUser(appUser);
 
        //Roles autorizados para el usuario
        List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
        if (roles != null && !roles.isEmpty()) {
            for (UserRole role : roles) {
                GrantedAuthority authority = new SimpleGrantedAuthority(role.getAppRole().getRoleName());
                grantList.add(authority);
            }//Fin for
        }//Fin if
 
        //
        UserDetails userDetails = (UserDetails) new User(appUser.getUserName(), appUser.getEncrytedPassword(), grantList);

        return userDetails;
	}//Fin loadUserByUsername
}//Fin UserDetailsServiceImp