package com.iecisa.dob.santander.statistics.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iecisa.dob.santander.statistics.entity.AppUser;
import com.iecisa.dob.santander.statistics.entity.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
	
	/**
	 * Devuelve los roles asignados al usuario.
	 * @param userId
	 * @return Objeto Wrapper entity UserRole.
	 */
	List<UserRole> findByAppUser(AppUser appUser);

}
