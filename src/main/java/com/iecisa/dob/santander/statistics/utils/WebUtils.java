package com.iecisa.dob.santander.statistics.utils;

import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class WebUtils {
	
	/**
	 * Devuelve String con información del usuario
	 * @param user
	 * @return
	 */
	public static String toString(User user) {
		StringBuilder sb = new StringBuilder();
		Collection<GrantedAuthority> authorities = user.getAuthorities();	
		if (authorities != null && !authorities.isEmpty()) {
			sb.append("<br>Roles asociados: (");
			boolean first = true;
			
			for (GrantedAuthority a : authorities) {
				if (first) {
					sb.append(a.getAuthority());
					first = false;
				} else {
					sb.append(", ").append(a.getAuthority());
				}
			}//Fin for
			sb.append(")");
		}//Fin if
		return sb.toString();
	}//Fin toString
}//Fin WebUtils
