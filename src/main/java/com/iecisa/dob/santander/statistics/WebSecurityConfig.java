package com.iecisa.dob.santander.statistics;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import com.iecisa.dob.santander.statistics.security.rememberme.JdbcTokenRepositoryImpl;
import com.iecisa.dob.santander.statistics.services.impl.UserDetailsServiceImp;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
    private DataSource dataSource;
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	
    	http.csrf().disable();
    	
    	//Se permite el acceso a Login y Logout (No es necesario authenticación)
    	http.authorizeRequests().antMatchers("/", "/login", "/logout").permitAll();

    	//Peticiones autorizadas para los siguientes roles 'ROLE_ADMIN' y 'ROLE_USER'.
    	http.authorizeRequests().antMatchers("/browser" , "/browserAjax").access("hasAnyRole('ROLE_ADMIN','ROLE_USER')");

    	//Peticiones autorizadas solo para el rol 'ROLE_ADMIN'.
    	http.authorizeRequests().antMatchers("/statistics" , "/graphics").access("hasRole('ROLE_ADMIN')");
    	
    	/* *********************************************************************************
		 * Si un usuario con rol 'ROLE_USER' intenta acceder a una URL que solo esta       *
		 * autorizada para el rol ROLE_ADMIN, se lanza 403.                                *
		 * *********************************************************************************
		 * Un servidor web puede devolver un código de estado HTTP 403 Forbidden 		   *
		 * (Prohibido) en respuesta a un cliente de una página web o servicio para		   *
		 * indicar que el servidor se niega a permitir la acción solicitada. En otras	   *
		 * palabras, el servidor ha podido ser contactado, y ha recibido una petición	   *
		 * válida, pero ha denegado el acceso a la acción que se solicita.				   *
		 * *********************************************************************************
		 */
    	http.authorizeRequests().and().exceptionHandling().accessDeniedPage("/403");
    	
    	//Regla de configuración para el formulario de Login.
		http.authorizeRequests().and().formLogin().loginProcessingUrl("/j_spring_security_check").loginPage("/login")
				.defaultSuccessUrl("/browser").failureUrl("/login?error=true").usernameParameter("username")
				.passwordParameter("password").and().logout().logoutUrl("/logout")
				.logoutSuccessUrl("/logoutSuccessful");
 
        // Configuración de Remember Me.
        http.authorizeRequests().and() //
                .rememberMe().tokenRepository(this.persistentTokenRepository()) //
                .tokenValiditySeconds(1 * 24 * 60 * 60); // 24h
    }

    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl db = new JdbcTokenRepositoryImpl();
        db.setDataSource(dataSource);
        return db;
    }
    
    @Bean
    public UserDetailsService userDetailsService() {
      return new UserDetailsServiceImp();
    };
    
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
      return new BCryptPasswordEncoder();
    };
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
      auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder());
    }
}