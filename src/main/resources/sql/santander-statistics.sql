-- Create table
create table public.app_user
(
  user_id           BIGINT not null,
  user_name         VARCHAR(36) not null,
  encrypted_password VARCHAR(128) not null,
  enabled           Int not null 
) ;
--  
alter table public.app_user
  add constraint app_user_PK primary key (user_id);
 
alter table public.app_user
  add constraint app_user_UK unique (user_name);
 
 
-- Create table
create table public.app_role
(
  role_id   BIGINT not null,
  role_name VARCHAR(30) not null
) ;
--  
alter table public.app_role
  add constraint app_role_PK primary key (role_id);
 
alter table public.app_role
  add constraint app_role_UK unique (role_name);
 
 
-- Create table
create table public.user_role
(
  id      BIGINT not null,
  user_id BIGINT not null,
  role_id BIGINT not null
);
--  user_role
alter table public.user_role
  add constraint user_role_PK primary key (id);
 
alter table public.user_role
  add constraint user_role_UK unique (user_id, role_id);
 
alter table public.user_role
  add constraint user_role_FK1 foreign key (user_id)
  references public.app_user (user_id);
 
alter table public.user_role
  add constraint user_role_FK2 foreign key (role_id)
  references public.app_role (role_id);
 
  
  
-- Used by Spring Remember Me API.  
CREATE TABLE public.persistent_logins (
 
    username varchar(64) not null,
    series varchar(64) not null,
    token varchar(64) not null,
    last_used timestamp not null,
    PRIMARY KEY (series)
     
);
  
--------------------------------------
 
insert into public.app_user (user_id, user_name, encrypted_password, enabled)
values (2, 'dbuser1', '$2a$10$PrI5Gk9L.tSZiW9FXhTS8O8Mz9E97k2FZbFvGFFaSsiTUIl.TCrFu', 1);
 
insert into public.app_user (user_id, user_name, encrypted_password, enabled)
values (1, 'dbadmin1', '$2a$10$PrI5Gk9L.tSZiW9FXhTS8O8Mz9E97k2FZbFvGFFaSsiTUIl.TCrFu', 1);
 
---
 
insert into public.app_role (role_id, role_name)
values (1, 'ROLE_ADMIN');
 
insert into public.app_role (role_id, role_name)
values (2, 'ROLE_USER');
 
---
 
insert into public.user_role (id, user_id, role_id)
values (1, 1, 1);
 
insert into public.user_role (id, user_id, role_id)
values (2, 1, 2);
 
insert into public.user_role (id, user_id, role_id)
values (3, 2, 2);
