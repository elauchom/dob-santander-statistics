$(document).ready(function() {
	setInterval(loadData(), 60000);
});

function getBaseUrl() {
    return window.location.href.match(/^.*\//);
}

function loadData() {

	var peticion = getBaseUrl() + "statistics/graphics/ajax";
	//alert(peticion)
	$.ajax({
		type : "GET",
		contentType : "application/json",
		url : peticion,
		data : [],
		dataType : 'json',
		cache : false,
		timeout : 300000,
		headers : {
			'Content-type' : 'application/json'
		},
		success : function(response) {

			var dom = document.getElementById("graphics");
			var myChart = echarts.init(dom);
			var app = {};
			var option = null;
			option = {
				    legend: {},
				    tooltip: {},
				    dataset: {
				        source: [
				            ['product', 'Registros' , 'En tiempo', 'Sin respuesta', 'Media archivos' , 'Media páginas' , 'Tiempo medio' ,'Porcentaje <= 60 Seg.' , 'Porcentaje sin respuesta'],
				            ['Stats', 
				            	response.total ,
				            	response.resultasMenos60Seg ,
				            	response.documentResultNull , 
				            	response.mediaFiles , 
				            	response.mediaPages, 
				            	response.mediaSecondsPerTransaction, 
				            	response.porcentajeTiempoEstablecido, 
				            	response.porcentajeDocumentResultNull
				            ]
				        ]
				    },
				    xAxis: {type: 'category'},
				    yAxis: {},
				    // Declare several bar series, each will be mapped
				    // to a column of dataset.source by default.
				    series: [
				        {type: 'bar'},
				        {type: 'bar'},
				        {type: 'bar'},
				        {type: 'bar'},
				        {type: 'bar'},
				        {type: 'bar'},
				        {type: 'bar'},
				        {type: 'bar'}
				    ]
			};
			if (option && typeof option === "object") {
				//Se establecen los valores de las opciones al gráfico.
				myChart.setOption(option, true);
			}

		},
		error : function(e) {

			var json = "<h4>Ajax Response</h4><pre>" + e.responseText
					+ "</pre>";
			$('#feedback').html(json);

			console.log("ERROR : ", e);
			$("#btn-statistics").prop("disabled", false);

		}
	});

};
