//Equivalente al onload.
$(document).ready(function() {

	var fechaActual = moment(); //Get the current date
	fechaActual.format("YYYY-MM-DD"); //2014-07-10
	$('#fechaDesde').val(fechaActual.format("YYYY-MM-DD"));
	$('#fechaHasta').val(fechaActual.format("YYYY-MM-DD"));
	
	$("#search-form").submit(function (event) {
        event.preventDefault();
    });	
	
	//Carga por primera vez las estadisticas
	getStatisticsAjax();
});

//Ajax request
function getStatisticsAjax() 
{	
	//Se muestra imagen cargando.
	$(".loader").show();
	
	//Tratamiento de fechas.
	var fechaDesde = document.getElementById("fechaDesde");
	var fechaHasta = document.getElementById("fechaHasta");
	moment.locale('es-ES');
	var fDesde = moment(fechaDesde.value, "YYYY-MM-DD").format("YYYY-MM-DD HH:mm:ss");
	var fHasta = moment(fechaHasta.value, "YYYY-MM-DD").format("YYYY-MM-DD HH:mm:ss");
	
	var data = {
			fechaDesde : fDesde,
			fechaHasta : fHasta,
	};

    $('#statisticsAjax').load( getBaseUrl() + "statistics/ajax" , data, 
    		function( response, status, xhr ) 
    		{
    			if ( status == "error" ) {
    			    var msg = "Ha ocurrido un error en la petición: ";
    			    $( "#error" ).html( msg + xhr.status + " " + xhr.statusText );
    			} 
    			
    			//Se quita la imagen de cargando.
				 $(".loader").fadeOut("slow");
    		}
    );
}//fin getStatisticsAjax

//Obtiene el URL base de la aplicación.
function getBaseUrl() {
    return window.location.href.match(/^.*\//);
}